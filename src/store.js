import { observable } from 'mobx';

const store = observable({
	ticks: 0, // Game logic iterations
	cash: 0, // Current cash
	officeSpace: 4, // Number of available desks
	features: [
		{
			type: 'foo',
			quality: 9,
			complexity: 9,
			progress: 90,
			bugs: 8,
		}
	],
	mods: [
		{
			ts: 12124125125,
			type: 'bar',
		}
	],
	positions: [
		{
			ts: 12312415125,
			type: 'rockstar',
			xp: 0,
			rank: 1,
			assignment: 'foo',
		}
	],

	// --- UI Specific:
	modals: []
});

export default store;