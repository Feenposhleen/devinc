import store from './store';

export const tick = number =>
	store.ticks += number;

export const openModal = text =>
	store.modals.push({ text: text })

export const closeModal = () =>
	store.modals.shift()
