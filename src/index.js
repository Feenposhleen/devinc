import React from 'react';
import ReactDOM from 'react-dom';
import Index from './components/Index';
import store from './store';
import * as actions from './actions';
import './style/global.css';

ReactDOM.render(<Index />, document.getElementById('root'));
setInterval(() => actions.tick(1), 1000);

window.store = store;
window.actions = actions;