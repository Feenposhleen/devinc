export const basicVideoService = {
	time: 20,
	complexity: 1,
	revenue: 1,
	roles: [ 'frontend', 'backend' ]
};

export const simpleVideoPlayer = {
	time: 20,
	complexity: 1,
	revenue: 1,
	roles: [ 'frontend' ]
};

export const ingestPipeline = {
	time: 20,
	complexity: 1,
	revenue: 1,
	roles: [ 'backend' ]
};