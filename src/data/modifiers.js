export const energyDrink = {
	duration: 20,
	type: 'speed',
	modifier: 1.5,
};

export const standingDesk = {
	duration: Infinity,
	type: 'quality',
	modifier: 1.2,
};
