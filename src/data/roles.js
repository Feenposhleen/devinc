export const fullStack = {
	quality: 5,
	complexity: 5,
	speed: 5,
	teamwork: 5,
	roles: [],
};

export const frontend = {
	quality: 5,
	complexity: 5,
	speed: 5,
	teamwork: 5,
	roles: [ 'frontend' ],
};
