import React from 'react';
import { inject, observer } from 'mobx-react';
import '../style/Debug.css';

const Debug = ({ store }) => (
	<div className="Debug">
		<textarea
			className="Debug-area"
			readOnly
			value={ JSON.stringify(store, null, 2) }
		/>
	</div>
);

export default inject('store')(
	observer(Debug)
);
