import React from 'react';
import { Provider } from 'mobx-react';
import store from '../store';
import * as actions from '../actions';
import '../style/global.css';
import App from './App';
import Debug from './Debug';

const Index = () => (
	<Provider store={ store } actions={ actions }>
		{
			window.location.hash === '#debug'
				? <Debug />
				: <App />
		}
	</Provider>
);

export default Index;
