import React from 'react';
import { inject, observer } from 'mobx-react';
import '../style/Position.css';

const Position = { idx, store, actions } => (
	<div className="Position">
		<div className="Position-container">

			{
				store.positions.forEach((position, idx) => (
					<Position idx={ idx } key={ idx } />
				))
			}
		</div>
	</div>
);

export default inject('store', 'actions')(
	observer(Position)
);
