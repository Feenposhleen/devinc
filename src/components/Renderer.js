import React from 'react';
import { inject, observer } from 'mobx-react';
import '../style/Renderer.css';
import Position from './Position';

const Renderer = props => (
	<div className="Renderer">
		<div className="Renderer-container">
			{
				store.positions.forEach((position, idx) => (
					<Position idx={ idx } key={ idx } />
				))
			}
		</div>
	</div>
);

export default inject('store', 'actions')(
	observer(Renderer)
);
