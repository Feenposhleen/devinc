import React from 'react';
import { inject, observer } from 'mobx-react';
import '../style/App.css';
import CSSTransitionGroup from 'react-addons-css-transition-group';

const App = ({ store, actions }) => (
		<div className="App">
			<h1 className="App-header">Ticker</h1>
			<p className="App-ticker">
				{ store.ticks }
			</p>
			<p>Look, just because I don't be givin' no man a foot massage don't make it right for Marsellus to throw Antwone into a glass motherfuckin' house, fuckin' up the way the nigger talks. Motherfucker do that shit to me, he better paralyze my ass, 'cause I'll kill the motherfucker, know what I'm sayin'?</p>
			<button onClick={ () => actions.openModal('Hej') }>Open Hej</button>

			<CSSTransitionGroup
				transitionName="fade"
				transitionEnterTimeout={ 400 }
				transitionLeaveTimeout={ 400 }
			>
				{
					!!store.modals.length && (
						<div className="App-modal" key="modal" onClick={ () => actions.closeModal() }>
							{ store.modals[0].text }
						</div>
					)
				}
			</CSSTransitionGroup>
		</div>
);

App.propTypes = {
	store: React.PropTypes.object.isRequired,
	actions: React.PropTypes.object.isRequired
};

export default inject('store', 'actions')(
	observer(App)
);
